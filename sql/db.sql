CREATE TABLE IF NOT EXISTS `users`
(
    `id`       INT          NOT NULL AUTO_INCREMENT,
    `username` varchar(50)  NOT NULL,
    `enabled`  bit(1)       NOT NULL,
    `password` varchar(100) NOT NULL,
    PRIMARY KEY (`id`),
    constraint `unique_username` unique (`username`)
);

CREATE TABLE IF NOT EXISTS `authorities`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    PRIMARY KEY (`id`)
);


CREATE TABLE IF NOT EXISTS `users_authorities`
(
    `id_users`     INT        NOT NULL,
    `authority_id` INT NOT NULL,
    CONSTRAINT `FK_authority_id` FOREIGN KEY (`authority_id`) REFERENCES `authorities` (`id`),
    CONSTRAINT `FK_id_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
);

CREATE TABLE IF NOT EXISTS `users_detail`
(
    `user_id`       INT        NOT NULL,
    `name`          varchar(50),
    `surname`       varchar(100),
    `image`         varchar(250),
    `mail`          varchar(150),
    `cf`            varchar(20),
    `birth_date`    date,
    `phone`         varchar(15),
    `token_mobile`  varchar(200),
    `primo_accesso` bit(1)              default 0,
    `admin_user_id` INT NULL,
    PRIMARY KEY (`user_id`),
    CONSTRAINT `FK_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

CREATE TABLE IF NOT EXISTS `tip_garanzia`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `descrizione` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
);
CREATE TABLE IF NOT EXISTS `tip_firma`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `descrizione` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
);
CREATE TABLE IF NOT EXISTS `tip_cliente`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `descrizione` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `tip_documento`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `descrizione` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
);


CREATE TABLE IF NOT EXISTS `pratica`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `id_tip_garanzia` INT NULL,
    `oggetto`       varchar(250),
    `note`         varchar(250),
    `valido_da`    date,
    `valido_a`    date,
    `durata_val`    INT,
    `ext_garanzia_da`    date,
    `ext_garanzia_a`    date,
    `durata_ext`    INT,
    `importo`       float default 0,
    `tasso`    float default 0,
    `premio`    float default 0,
    `valore_progetto`    float default 0,
    `id_tip_firma` INT NULL,
    `id_tip_stato` INT NULL,
    `id_owner_user`  INT NOT NULL,
    `stato` INT NOT NULL DEFAULT 0,
    `ultima_mod`    datetime,
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_firma` FOREIGN KEY (`id_tip_firma`) REFERENCES `tip_firma` (`id`),
    CONSTRAINT `FK_garanzia` FOREIGN KEY (`id_tip_garanzia`) REFERENCES `tip_garanzia` (`id`),
    CONSTRAINT `FK_owner_id` FOREIGN KEY (`id_owner_user`) REFERENCES `users` (`id`),
    CONSTRAINT `FK_stato` FOREIGN KEY (`id_tip_stato`) REFERENCES `tip_stato` (`id`)
);
CREATE TABLE IF NOT EXISTS `cliente`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `id_tip_cliente` INT NULL,
    `nome`       varchar(250),
    `cognome`         varchar(250),
    `ragione_sociale`       varchar(250),
    `piva`         varchar(20),
    `cf`         varchar(20),
    `pec`       varchar(100),
    `indirizzo` varchar(250),
    `civico`    varchar(10),
    `citta`    varchar(80),
    `provincia` varchar(80),
    `cap`    varchar(10),
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_tip_cliente_cliente` FOREIGN KEY (`id_tip_cliente`) REFERENCES `tip_cliente` (`id`)
);
CREATE TABLE IF NOT EXISTS `cliente_pratica`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `id_pratica`     INT        NOT NULL,
    `id_cliente` INT NOT NULL,
    `id_tip_cliente` INT NOT NULL,
    `stato` INT NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_pratica_id` FOREIGN KEY (`id_pratica`) REFERENCES `pratica` (`id`),
    CONSTRAINT `FK_cliente_id` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
    CONSTRAINT `FK_cliente_pratica_tip_cliente` FOREIGN KEY (`id_tip_cliente`) REFERENCES `tip_cliente` (`id`)
);

CREATE TABLE IF NOT EXISTS `documento_pratica`
(
    `id`        INT   NOT NULL AUTO_INCREMENT,
    `tipo_doc`     varchar(255) not null,
    `id_pratica` INT NOT NULL,
    `id_tip_documento` INT NOT NULL,
    `filename`     varchar(255) not null,
    `path_doc`     varchar(255) not null,
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_documento_pratica_id` FOREIGN KEY (`id_pratica`) REFERENCES `pratica` (`id`),
    CONSTRAINT `FK_documento_pratica_tip_documento` FOREIGN KEY (`id_tip_documento`) REFERENCES `tip_documento` (`id`)

);

CREATE TABLE IF NOT EXISTS `documento_cliente`
(
    `id`        INT   NOT NULL AUTO_INCREMENT,
    `tipo_doc`     varchar(255) not null,
    `id_cliente` INT NOT NULL,
    `id_tip_documento` INT NOT NULL,
    `filename`     varchar(255) not null,
    `path_doc`     varchar(255) not null,
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_documento_cliente_id` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
    CONSTRAINT `FK_documento_cliente_tip_documento` FOREIGN KEY (`id_tip_documento`) REFERENCES `tip_documento` (`id`)

);


INSERT INTO `authorities` (`id`, `name`)
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_USER');
INSERT INTO `tip_garanzia` (id, descrizione)
VALUES (1,'Contributi Pubblici - Corretta Esecuzione'),
(2, 'Contributi Pubblici - Erogazione Anticipazione'),
(3, 'Contributi Pubblici - Erogazione Saldo'),
(4, 'Definitiva'),
(5, 'Garanzia Provvisoria'),
(6, 'Generica Enti Pubblici'),
(7, 'Generica Privati'),
(8, 'Lavori per enti pubblici - Erogazione Anticipazione'),
(9, 'Legge 210 del 02.08.2004 / D.L. 122/2005'),
(10, 'Oneri Urbanizzazione'),
(11, 'Provvisoria'),
(12, 'Rimborso IVA Gruppo'),
(13, 'Rimborso IVA annuale'),
(14, 'Rimborso IVA infrannuale');
INSERT INTO `tip_firma` (id, descrizione)
VALUES (1,'Digitale (firma certificata)'),
(2,'Digitale con autentica notarile digitale'),
(3,'Originale cartaceo con autentica notarile'),
(4,'Originale cartaceo con autentica notarile e apostilla'),
(5,'Originale cartaceo');
INSERT INTO `tip_cliente` (id, descrizione)
VALUES (1,'CONTRAENTE'),
       (2, 'BENEFICIARIO'),
       (3, 'COOBBLIGATO');
INSERT INTO `tip_documento` (id, descrizione)
VALUES (1, 'DOC_TECNICA'),
       (2, 'DOC_IDEN'),
       (3, 'DIC_REDDITI'),
       (4, 'DOC_IDEN_RESP'),
       (5, 'BIL_COMPL_UEC'),
       (6, 'BIL_PROV'),
       (7, 'VIS_CAT'),
       (8, 'VIS_CAM'),
       (9, 'ALTRO');
CREATE TABLE IF NOT EXISTS `tip_stato`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `descrizione` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
);
INSERT INTO `tip_stato` (id, descrizione)
VALUES (1, 'INSERITA'),
       (2, 'INTEGRARE'),
       (3, 'ISTRUTTORIA'),
       (4, 'DELIBERA'),
       (5, 'ACCETTATA'),
       (6, 'RESPINTA');

alter table pratica
    add id_tip_stato int default 1 null after id_tip_firma;
alter table pratica
    add constraint FK_stato
        foreign key (id_tip_stato) references tip_stato (id);



