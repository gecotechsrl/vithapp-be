CREATE TABLE IF NOT EXISTS `provincies` (
`sigla` varchar(2) NOT NULL,
`provincia` varchar(255) DEFAULT NULL,
`id_regione` smallint(6) DEFAULT NULL,
PRIMARY KEY (`sigla`)
);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('AG','Agrigento',15),
	 ('AL','Alessandria',12),
	 ('AN','Ancona',10),
	 ('AO','Aosta',19),
	 ('AP','Ascoli Piceno',10),
	 ('AQ','L''Aquila',1),
	 ('AR','Arezzo',16),
	 ('AT','Asti',12),
	 ('AV','Avellino',4),
	 ('BA','Bari',13);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('BG','Bergamo',9),
	 ('BI','Biella',12),
	 ('BL','Belluno',20),
	 ('BN','Benevento',4),
	 ('BO','Bologna',5),
	 ('BR','Brindisi',13),
	 ('BS','Brescia',9),
	 ('BT','Barletta-Andria-Trani',13),
	 ('BZ','Bolzano',17),
	 ('CA','Cagliari',14);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('CB','Campobasso',11),
	 ('CE','Caserta',4),
	 ('CH','Chieti',1),
	 ('CL','Caltanissetta',15),
	 ('CN','Cuneo',12),
	 ('CO','Como',9),
	 ('CR','Cremona',9),
	 ('CS','Cosenza',3),
	 ('CT','Catania',15),
	 ('CZ','Catanzaro',3);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('EN','Enna',15),
	 ('FC','Forlì-Cesena',5),
	 ('FE','Ferrara',5),
	 ('FG','Foggia',13),
	 ('FI','Firenze',16),
	 ('FM','Fermo',10),
	 ('FR','Frosinone',7),
	 ('GE','Genova',8),
	 ('GO','Gorizia',6),
	 ('GR','Grosseto',16);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('IM','Imperia',8),
	 ('IS','Isernia',11),
	 ('KR','Crotone',3),
	 ('LC','Lecco',9),
	 ('LE','Lecce',13),
	 ('LI','Livorno',16),
	 ('LO','Lodi',9),
	 ('LT','Latina',7),
	 ('LU','Lucca',16),
	 ('MB','Monza e della Brianza',9);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('MC','Macerata',10),
	 ('ME','Messina',15),
	 ('MI','Milano',9),
	 ('MN','Mantova',9),
	 ('MO','Modena',5),
	 ('MS','Massa e Carrara',16),
	 ('MT','Matera',2),
	 ('NA','Napoli',4),
	 ('NO','Novara',12),
	 ('NU','Nuoro',14);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('OR','Oristano',14),
	 ('PA','Palermo',15),
	 ('PC','Piacenza',5),
	 ('PD','Padova',20),
	 ('PE','Pescara',1),
	 ('PG','Perugia',18),
	 ('PI','Pisa',16),
	 ('PN','Pordenone',6),
	 ('PO','Prato',16),
	 ('PR','Parma',5);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('PT','Pistoia',16),
	 ('PU','Pesaro e Urbino',10),
	 ('PV','Pavia',9),
	 ('PZ','Potenza',2),
	 ('RA','Ravenna',5),
	 ('RC','Reggio Calabria',3),
	 ('RE','Reggio Emilia',5),
	 ('RG','Ragusa',15),
	 ('RI','Rieti',7),
	 ('RM','Roma',7);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('RN','Rimini',5),
	 ('RO','Rovigo',20),
	 ('SA','Salerno',4),
	 ('SI','Siena',16),
	 ('SO','Sondrio',9),
	 ('SP','La spezia',8),
	 ('SR','Siracusa',15),
	 ('SS','Sassari',14),
	 ('SU','Sud Sardegna',14),
	 ('SV','Savona',8);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('TA','Taranto',13),
	 ('TE','Teramo',1),
	 ('TN','Trento',17),
	 ('TO','Torino',12),
	 ('TP','Trapani',15),
	 ('TR','Terni',18),
	 ('TS','Trieste',6),
	 ('TV','Treviso',20),
	 ('UD','Udine',6),
	 ('VA','Varese',9);
INSERT INTO provincies (sigla,provincia,id_regione) VALUES
	 ('VB','Verbano Cusio Ossola',12),
	 ('VC','Vercelli',12),
	 ('VE','Venezia',20),
	 ('VI','Vicenza',20),
	 ('VR','Verona',20),
	 ('VT','Viterbo',7),
	 ('VV','Vibo Valentia',3);
