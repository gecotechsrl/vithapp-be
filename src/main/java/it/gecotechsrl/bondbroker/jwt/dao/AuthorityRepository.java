package it.gecotechsrl.bondbroker.jwt.dao;

import it.gecotechsrl.bondbroker.jwt.model.Authority;
import it.gecotechsrl.bondbroker.jwt.model.AuthorityName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Integer> {
    Authority findByName(AuthorityName name);
}
