package it.gecotechsrl.bondbroker.jwt.dao;

import it.gecotechsrl.bondbroker.jwt.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);
    List<User> findByUsersDetail_AdminUserId(Integer adminId);
    List<User> findByAuthorities_id(Integer authorityId);
    @Query(value = "select user_id from users_detail where admin_user_id= ?1", nativeQuery = true)
    List<Integer> getListUserIdByAdminUserId(Integer adminId);
}
