package it.gecotechsrl.bondbroker.jwt;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import it.gecotechsrl.bondbroker.dto.UserAppReq;
import it.gecotechsrl.bondbroker.jwt.dao.AuthorityRepository;
import it.gecotechsrl.bondbroker.jwt.dao.UserRepository;
import it.gecotechsrl.bondbroker.jwt.model.AuthorityName;
import it.gecotechsrl.bondbroker.jwt.model.User;
import it.gecotechsrl.bondbroker.jwt.model.UsersDetail;
import it.gecotechsrl.bondbroker.jwt.security.JwtTokenUtil;
import it.gecotechsrl.bondbroker.jwt.security.dto.UserResponse;
import it.gecotechsrl.bondbroker.jwt.service.JwtUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Optional;

@RestController
public class AuthenticationRestController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthorityRepository authorityRepository;

    /*@Autowired
    AsyncGetReport asyncGetReport;
*/
    private Logger logger = LoggerFactory.getLogger(AuthenticationRestController.class);


    @PostMapping(value = "public/login")
    @Transactional
    public ResponseEntity createAuthenticationTokenAdd(@RequestBody UserAppReq userAppReq,
                                                       HttpServletResponse response) {
        boolean checkOk = false;
        UserRecord userRecord = null;
        if (Objects.isNull(userAppReq.getUid()) && (Objects.isNull(userAppReq.getMail()) && Objects.isNull(userAppReq.getPassword())))
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("UID / Email e Password missing !!!");
        try {
            userRecord = FirebaseAuth.getInstance().getUser(userAppReq.getUid());
            if (Objects.nonNull(userRecord.getDisplayName())) {
                String name = userRecord.getDisplayName().substring(0, userRecord.getDisplayName().indexOf(" "));
                String surname = userRecord.getDisplayName().length() > name.length() + 1 ? userRecord.getDisplayName().substring(name.length() + 1) : "";
                userAppReq.setName(name);
                userAppReq.setSurname(surname);
            }
            if (Objects.nonNull(userRecord.getPhoneNumber())) {
                userAppReq.setPhone(userRecord.getPhoneNumber());
            }
            if (Objects.nonNull(userRecord.getEmail())) {
                userAppReq.setMail(userRecord.getEmail());
            }
            Optional<User> logUser = userRepository.findByUsername(userRecord.getUid());
            User user = logUser.isPresent() ? logUser.get() : addUser(userRecord.getUid(), userAppReq);
            final Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(userRecord.getUid(),
                            userRecord.getUid()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(userRecord.getUid());
            final String token = jwtTokenUtil.generateToken(userDetails);
            response.setHeader(tokenHeader, token);
            UserResponse userResponse = new UserResponse(user);
            userResponse.setExp(jwtTokenUtil.getExpirationDateFromToken(token).getTime());
            return ResponseEntity.ok(userResponse);
        } catch (FirebaseAuthException e) {
            String errorMsg = e.getMessage() + "-" + e.getErrorCode();
            logger.error("FirebaseAuthException {}", errorMsg);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMsg);
        } catch (DisabledException disabledException) {
            logger.error("disabledException Exception", disabledException);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } catch (Exception e) {
            logger.error("Generic Exception", e);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private User addUser(String uid, UserAppReq userReq) {
        User user = new User();
        user.setEnabled(true);
        user.setUsername(uid);
        user.setPassword(passwordEncoder.encode(uid));
        if (Objects.nonNull(userReq.getAuthorityName()))
            user.addAuthority(authorityRepository.findByName(AuthorityName.valueOf(userReq.getAuthorityName())));
        else
            user.addAuthority(authorityRepository.findByName(AuthorityName.ROLE_USER));
        UsersDetail usersDetail = new UsersDetail();
        if (Objects.nonNull(userReq.getName())) usersDetail.setName(userReq.getName());
        if (Objects.nonNull(userReq.getSurname())) usersDetail.setSurname(userReq.getSurname());
        if (Objects.nonNull(userReq.getBirthDate())) usersDetail.setDataNascita(userReq.getBirthDate());
        if (Objects.nonNull(userReq.getCf())) usersDetail.setCf(userReq.getCf());
        if (Objects.nonNull(userReq.getMail())) usersDetail.setMail(userReq.getMail());
        if (Objects.nonNull(userReq.getPhone())) usersDetail.setPhone(userReq.getPhone());
        usersDetail.setPrimoAccesso(true);
        usersDetail.setUser(user);
        user.setUsersDetail(usersDetail);
        user = userRepository.save(user);
        return user;
    }

    @GetMapping(value = "protected/refresh-token")
    public ResponseEntity refreshAndGetAuthenticationToken(HttpServletRequest request,
                                                           HttpServletResponse response) {
        String token = request.getHeader(tokenHeader);
        if (Boolean.TRUE.equals(jwtTokenUtil.canTokenBeRefreshed(token))) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            response.setHeader(tokenHeader, refreshedToken);
//            logger.info("RefreshToken for : {}", jwtTokenUtil.getUsernameFromToken(refreshedToken));
            return ResponseEntity.ok(jwtTokenUtil.getResponseToken(refreshedToken));
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("TOKEN EXPIRED !!!");
        }
    }
    /*@GetMapping("public/getReportExcel/{dataReport}/{idUser}/{adminControllo}")
    public void getReportExcel(@PathVariable String dataReport, HttpServletResponse response,@PathVariable Integer idUser, @PathVariable Integer adminControllo) throws IOException, ExecutionException, InterruptedException {
        LocalDate localDate = LocalDate.parse(dataReport, DateTimeFormatter.ofPattern("yyyyMMdd"));
        response.setHeader(
                HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"report" + dataReport + ".xlsx\"");
        asyncGetReport.getReportIdAgente(localDate, response.getOutputStream(), idUser, adminControllo == 0 ? false : true).get().flush();
    }*/

}
