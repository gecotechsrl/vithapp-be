package it.gecotechsrl.bondbroker.jwt.security.dto;

import it.gecotechsrl.bondbroker.jwt.model.Authority;
import it.gecotechsrl.bondbroker.jwt.model.User;
import it.gecotechsrl.bondbroker.jwt.model.UsersDetail;
import lombok.Data;

import java.util.List;


@Data
public class UserResponse {
    private int id;
    private String username;
    private UsersDetail usersDetail;
    private List<Authority> authorities;
    private long exp;

    public UserResponse (User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.usersDetail = user.getUsersDetail();
        this.authorities = user.getAuthorities();
    }
}
