package it.gecotechsrl.bondbroker.jwt.security.dto;

import java.util.List;

public class ResponseToken {
    private String username;
    private List<String> authorities;
    private long exp;

    public ResponseToken(String username, List<String> authorities, long exp) {
        this.username = username;
        this.authorities = authorities;
        this.exp = exp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }
}
