package it.gecotechsrl.bondbroker.jwt.model;

public enum AuthorityName {
    ROLE_ADMIN,ROLE_USER
}
