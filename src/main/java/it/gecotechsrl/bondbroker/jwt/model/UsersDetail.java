package it.gecotechsrl.bondbroker.jwt.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


@Entity
@Table(name="users_detail")
@Data
public class UsersDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	private String name;
	private String surname;
	private String image;
	private String mail;
	private String cf;

	@Column(name="birth_date")
	private LocalDate dataNascita;

	private String phone;

	@Column(name="token_mobile")
	private String tokenMobile;

	@Column(name="primo_accesso")
	private boolean primoAccesso;

	@Column(name="admin_user_id")
	private Integer adminUserId;

	@OneToOne
	@MapsId
	@JsonBackReference
	private User user;
}
