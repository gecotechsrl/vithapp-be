package it.gecotechsrl.bondbroker.jwt.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="users")
@Data
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private boolean enabled;
	@JsonIgnore
	private String password;
	private String username;

	//uni-directional many-to-many association to Authority
	@ManyToMany
	@JoinTable(
		name="users_authorities"
		, joinColumns={
			@JoinColumn(name="id_users")
			}
		, inverseJoinColumns={
			@JoinColumn(name="authority_id")
			}
		)
	private List<Authority> authorities = new ArrayList<>();

	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
	private UsersDetail usersDetail;

	public User() {}

	public void addAuthority(Authority authority) {
		authorities.add(authority);
		authority.getUsers().add(this);
	}

	public void removeAuthority(Authority authority) {
		authorities.remove(authority);
		authority.getUsers().remove(this);
	}

}
