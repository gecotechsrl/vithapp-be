package it.gecotechsrl.bondbroker.jwt.service;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;

public class JwtAuthenticationResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String username;
    private final String displayName;
    private Collection<? extends GrantedAuthority> authorities;
    private final long exp;

    public JwtAuthenticationResponse(String username, String displayName, Collection<? extends GrantedAuthority> authorities, long exp) {
        this.username = username;
        this.authorities = authorities;
        this.displayName = displayName;
        this.exp = exp;
    }

    public String getUsername() {
        return this.username;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public long getExp() {
        return exp;
    }

    public String getDisplayName() {
        return displayName;
    }
}
