package it.gecotechsrl.bondbroker;

public final class Constants {
    public static final String EMPTY_STRING = "";
    public static final String WHITE_SPACE_STRING = " ";
    public static final String BENEFICIARIO_STRING = "(BEN.)";
    public static final String MANDANTE_STRING = "(MANDANTE)";
  
}
