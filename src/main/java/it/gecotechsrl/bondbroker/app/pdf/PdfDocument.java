package it.gecotechsrl.bondbroker.app.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import it.gecotechsrl.bondbroker.Constants;
import it.gecotechsrl.bondbroker.app.model.TipGaranzia;
import it.gecotechsrl.bondbroker.dto.ClienteDto;
import it.gecotechsrl.bondbroker.dto.PraticaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

public class PdfDocument {
    Logger log = LoggerFactory.getLogger(PdfDocument.class);
    OutputStream os;

    public PdfDocument(OutputStream os) {
        this.os = os;
    }

    private static void writeContent(PdfWriter writer, Document document, String html) throws IOException {
        InputStream is = new ByteArrayInputStream(html.getBytes());
        XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
        is.close();
    }

    public void creaPdf(PraticaDto praticaDto) {
        PdfWriter writer;

        Document document = new Document();

        try {
            document.setPageSize(PageSize.A4);
            document.setMargins(10, 10, 20, 20);
            writer = PdfWriter.getInstance(document, os);
            document.open();
            String html1 = startHtml()
                    .concat(headerLogo())
                    .concat(tipoGaranzia(praticaDto.getIdTipGaranzia()))
                    .concat(clienti(praticaDto.getContraente()))
                    .concat(clienti(praticaDto.getBeneficiario()))
                    .concat(clienti(praticaDto.getCoobbligato()))
                    .concat(oggetto(praticaDto.getOggetto()))
                    .concat(note(praticaDto.getNote()))
                    .concat(periodoValidita(praticaDto.getValidoDa(), praticaDto.getValidoA(), praticaDto.getDurataVal()))
                    .concat(estensioneGaranzia(praticaDto.getExtGaranziaDa(), praticaDto.getExtGaranziaA(), praticaDto.getDurataExt()))
                    .concat(tabImporto(praticaDto.getImporto(), praticaDto.getTasso()))
                    // .concat(tabPremio(praticaDto.getPremio()))
                    // .concat(testoDichiarazione())
                    .concat(endHtml());
            writeContent(writer, document, html1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }

    }

    private String loadLogoWithClassPathResource() {
        String pathlogo = null;
        try {
            pathlogo = new ClassPathResource("logo_top.png").getFile().getAbsolutePath();
        } catch (Exception ioException) {
            log.error(ioException.getMessage() + "Logo image not found");
        }
        return pathlogo;
    }


    private String startHtml() {
        return "<html>" +
                "<body style=\"width: 100%; padding: 10px; border: 1px solid #ddd; margin: 0 auto; font-family: 'arial'\">";
    }

    private String headerLogo() {
        String pathImg = loadLogoWithClassPathResource();
        pathImg = Objects.nonNull(pathImg) ? pathImg : "";

        String html = "<table width=\"50mm\" cellpadding=\"10\" cellspacing=\"1\" border=\"0\">" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"text-align: center;\">" +
                "<img src=\"" + pathImg + "\" style=\"width: 50mm\" />" +
                "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
        return html;
    }

    private String tipoGaranzia(TipGaranzia tipGaranzia) {
        String html = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:5px\">" +
                "<thead>" +
                "<tr>" +
                "<th style=\"border: 1px solid #000; text-align: left;font-weight: bold\">" +
                "Garanzia fideiussoria n." +
                "</th>" +
                "<th style=\"border: 1px solid #000; text-align: left;font-weight: bold\">" +
                "Rilasciata da" +
                "</th>" +
                "<th style=\"border: 1px solid #000; text-align: left;font-weight: bold\">" +
                "Cod" +
                "</th>" +
                "<th style=\"border: 1px solid #000; text-align: left;font-weight: bold\">" +
                "Tipologia garanzia" +
                "</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"border: 1px solid #000;\">-</td>" +
                "<td style=\"border: 1px solid #000;\">-</td>" +
                "<td style=\"border: 1px solid #000;\">-</td>" +
                "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(tipGaranzia) ? tipGaranzia.getDescrizione() : "-") + "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
        return html;
    }

    private String clienti(List<ClienteDto> listClienteDto) {
        if (!CollectionUtils.isEmpty(listClienteDto)) {
            String clienteTr = Constants.EMPTY_STRING;
            for (ClienteDto clienteDto : listClienteDto) {
                if (Objects.nonNull(clienteDto.getRagioneSociale()) || (Objects.nonNull(clienteDto.getNome()) && Objects.nonNull(clienteDto.getCognome()))) {
                    clienteTr = clienteTr.concat("<tr>" +
                            "<td colspan=\"2\" style=\"font-weight: bold; text-align: left; text-transform: uppercase; border: 1px solid #000;\">" +
                            (Objects.nonNull(clienteDto.getRagioneSociale()) ?
                                    clienteDto.getRagioneSociale() :
                                    clienteDto.getNome().concat(Constants.WHITE_SPACE_STRING).concat(clienteDto.getCognome())) +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(clienteDto.getPec()) ? clienteDto.getPec() : "-") + "</td>" +
                            "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(clienteDto.getPiva()) ? clienteDto.getPiva() : Objects.nonNull(clienteDto.getCf()) ? clienteDto.getCf() : "-") + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(clienteDto.getIndirizzo()) ? clienteDto.getIndirizzo().concat(Constants.WHITE_SPACE_STRING).concat(Objects.nonNull(clienteDto.getCivico()) ? clienteDto.getCivico() : Constants.EMPTY_STRING) : "-") + "</td>" +
                            "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(clienteDto.getCitta()) ? clienteDto.getCitta().concat(Constants.WHITE_SPACE_STRING).concat(Objects.nonNull(clienteDto.getProvincia()) ? clienteDto.getProvincia() : Constants.EMPTY_STRING) : "-") + "</td>" +
                            "</tr>");
                }
            }
            String html1 = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\"  width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                    "<thead>" +
                    "<tr>" +
                    "<th colspan=\"2\" style=\"text-align: center; font-weight: bold; border: 1px solid #000\">" +
                    listClienteDto.get(0).getIdTipCliente().getDescrizione() +
                    "</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>";
            String html2 = "</tbody>" +
                    "</table>";
            return clienteTr.equals(Constants.EMPTY_STRING) ? Constants.EMPTY_STRING : html1.concat(clienteTr).concat(html2);
        }
        return Constants.EMPTY_STRING;
    }

    private String oggetto(String oggetto) {
        String html = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                "<thead>" +
                "<tr>" +
                "<th style=\"border: 1px solid #000; text-align: center; font-weight: bold\">" +
                "Oggetto" +
                "</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(oggetto) ? oggetto : "-") + "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
        return html;
    }

    private String note(String note) {
        String html = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                "<thead>" +
                "<tr>" +
                "<th style=\"border: 1px solid #000;text-align: center; font-weight: bold\">" +
                "Condizioni particolari" +
                "</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(note) ? note : "-") + "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
        return html;
    }

    private String periodoValidita(LocalDate daData, LocalDate aData, int durata) {
        String html = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                "<thead>" +
                "<tr>" +
                "<th colspan=\"3\" style=\"text-align: center; font-weight: bold; border: 1px solid #000\">" +
                "Validità" +
                "</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(daData) ? "Da ".concat(daData.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))) : "-") + "</td>" +
                "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(aData) ? "A ".concat(aData.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))) : "-") + "</td>" +
                "<td style=\"border: 1px solid #000;\">" + ((durata > 0) ? durata : "-") + "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
        return (Objects.nonNull(daData) && Objects.nonNull(aData)) ? html : Constants.EMPTY_STRING;
    }

    private String estensioneGaranzia(LocalDate daData, LocalDate aData, int durata) {
        String html = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                "<thead>" +
                "<tr>" +
                "<th colspan=\"3\" style=\"text-align: center; font-weight: bold; border: 1px solid #000\">" +
                "Estensione garanzia" +
                "</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(daData) ? "Da ".concat(daData.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))) : "-") + "</td>" +
                "<td style=\"border: 1px solid #000;\">" + (Objects.nonNull(aData) ? "A ".concat(aData.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))) : "-") + "</td>" +
                "<td style=\"border: 1px solid #000;\">" + ((durata > 0) ? durata : "-") + "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
        return (Objects.nonNull(daData) && Objects.nonNull(aData)) ? html : Constants.EMPTY_STRING;
    }

    private String tabImporto(double importo, double tasso) {
        String html = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"font-weight: bold; border: 1px solid #000;\">Importo garantito</td>" +
                "<td style=\"border: 1px solid #000;\" colspan=\"2\">"+importo+"</td>" +
                "</tr>" +
                /*
                "<tr>" +
                "<td style=\"font-weight: bold; border: 1px solid #000;\">Al tasso lordo del</td>" +
                "<td style=\"border: 1px solid #000;\" colspan=\"2\">&nbsp;</td>" +
                "</tr>" +
                */
                "</tbody>" +
                "</table>";
        return html;
    }

    private String tabPremio(double premio) {
        double imponibile = premio * 0.125;
        double totale = premio + imponibile;
        String html = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                "<thead>" +
                "<tr>" +
                "<th colspan=\"6\" style=\"text-align: center; font-weight: bold; border: 1px solid #000\">" +
                "Premio" +
                "</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"border: 1px solid #000; font-weight: bold  font-size:12px;\">Netto</td>" +
                "<td style=\"border: 1px solid #000\">Accessori</td>" +
                "<td style=\"border: 1px solid #000\">Spese</td>" +
                "<td style=\"border: 1px solid #000\">Imponibile</td>" +
                "<td style=\"border: 1px solid #000\">Imposte</td>" +
                "<td style=\"border: 1px solid #000; font-weight: bold;  font-size:12px;\">Totale (Euro)</td>" +
                "</tr>" +
                "<tr>" +
                "<td style=\"border: 1px solid #000;\">&nbsp;</td>" +
                "<td style=\"border: 1px solid #000;\">0</td>" +
                "<td style=\"border: 1px solid #000;\"></td>" +
                "<td style=\"border: 1px solid #000;\">&nbsp;</td>" +
                "<td style=\"border: 1px solid #000;\">&nbsp;</td>" +
                "<td style=\"border: 1px solid #000;\">&nbsp;</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
        return html;
    }

    private String testoDichiarazione() {
        String html = "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"font-size:10px\">Dichiaro di aver ricevuto oggi ____ l’importo di ___(euro) a favore della compagnia___ </td>" +
                "<td style=\"font-weight: bold; text-align: center\">L'ESATTORE</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>" +
                "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:20px\">" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"font-size:10px\" colspan=\"2\">" +
                "La presente polizza è stata generat nel rispetto delle norme tecniche, dei criteri e dei requisiti stabiliti nel DPCM del 30.03.2009 e successive modifiche. La sua copia cartacea ha" +
                "valenza probatoria ai sensi dell’art. 16 del D. Lgs. Del 30/12/2010 n.235. La firma digitale del documento, la sua originalità e la corrispondenza del suo contenuto sono verificabili, secondo la regolamentazione definita da AgID (www.agid.gov.it), mediante accesso al sito istituzionale della compagnia.</td>" +
                "</tr>" +
                "</tbody></table>" +
                "<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" width=\"100%\" style= \"font-size:10px; margin-top:10px\">" +
                "<tbody>" +
                "<tr>" +
                "<td style=\"font-size:10px\">Emessa in _ esemplari ad un solo effetto il __/__/____ in ____<br/>Mod. bot100</td>" +
                "<td style=\"font-weight: bold; text-align: right\">ESEMPLARE PER IL BENEFICIARIO</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";

        return html;
    }

    private String endHtml() {
        String html = "</body></html>";
        return html;
    }


}



