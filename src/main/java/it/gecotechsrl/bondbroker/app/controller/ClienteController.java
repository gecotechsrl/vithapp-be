package it.gecotechsrl.bondbroker.app.controller;

import it.gecotechsrl.bondbroker.app.dao.ClientePraticaRepository;
import it.gecotechsrl.bondbroker.app.dao.ClienteRepository;
import it.gecotechsrl.bondbroker.app.dao.DocumentoClienteRepository;
import it.gecotechsrl.bondbroker.app.model.*;
import it.gecotechsrl.bondbroker.app.utils.ConverterUtils;
import it.gecotechsrl.bondbroker.dto.ClienteDto;
import it.gecotechsrl.bondbroker.dto.DocumentoDto;
import it.gecotechsrl.bondbroker.dto.PraticaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("protected/cliente/")
public class ClienteController {
    private static final Logger log = LoggerFactory.getLogger(ClienteController.class);
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ClientePraticaRepository clientePraticaRepository;
    @Autowired
    private DocumentoClienteRepository documentoClienteRepository;
    @Autowired
    private ConverterUtils converterUtils;


    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @GetMapping(value = "getClienteById/{idCliente}")
    public ResponseEntity<?> getClienteById(@PathVariable String idCliente) {
        ClienteDto clienteDto = new ClienteDto();
        try {
            Optional<Cliente> cliente = clienteRepository.findById(Integer.valueOf(idCliente));
            if (cliente.isPresent()) {
                clienteDto = converterUtils.convertCliente(cliente.get(), null);
            }
        } catch (Exception e) {
            log.error("GET CLIENTE BY ID : {}", e.getMessage(), e);
            return ResponseEntity.badRequest().body("CLIENTE NOT FIND !!!");
        }
        return ResponseEntity.ok(clienteDto);
    }

    @PostMapping(value = "addUpdateCliente/{idPratica}")
    @Transactional
    public ResponseEntity<?> addUpdateCliente(@RequestBody ClienteDto clienteDto, @PathVariable String idPratica) {
        Cliente cliente = convertCliente(clienteDto);
        if(Objects.isNull(clienteDto.getId())) {
            cliente = clienteRepository.save(cliente);
            clienteDto.setId(cliente.getId());
        }
        if (Objects.nonNull(clienteDto.getDocumentoIdentita())) clienteDto.setDocumentoIdentita(addDocumentoTypeCliente(clienteDto.getDocumentoIdentita(),setTipoDocumento(2), cliente));
        if (Objects.nonNull(clienteDto.getDichiarazioneRedditi())) clienteDto.setDichiarazioneRedditi(addDocumentoTypeCliente(clienteDto.getDichiarazioneRedditi(),setTipoDocumento(3), cliente));
        if (Objects.nonNull(clienteDto.getDocumentoIdentitaLegaleRappresentante())) clienteDto.setDocumentoIdentitaLegaleRappresentante(addDocumentoTypeCliente(clienteDto.getDocumentoIdentitaLegaleRappresentante(),setTipoDocumento(4), cliente));
        if (Objects.nonNull(clienteDto.getBilancioCompletoUltimoEsercizioChiuso())) clienteDto.setBilancioCompletoUltimoEsercizioChiuso(addDocumentoTypeCliente(clienteDto.getBilancioCompletoUltimoEsercizioChiuso(),setTipoDocumento(5), cliente));
        if (Objects.nonNull(clienteDto.getBilancioProvvisorioEsercizioCorso())) clienteDto.setBilancioProvvisorioEsercizioCorso(addDocumentoTypeCliente(clienteDto.getBilancioProvvisorioEsercizioCorso(),setTipoDocumento(6), cliente));
        if (Objects.nonNull(clienteDto.getVisuraCatastale())) clienteDto.setVisuraCatastale(addDocumentoTypeCliente(clienteDto.getVisuraCatastale(),setTipoDocumento(7), cliente));
        if (Objects.nonNull(clienteDto.getVisuraCamerale())) clienteDto.setVisuraCamerale(addDocumentoTypeCliente(clienteDto.getVisuraCamerale(),setTipoDocumento(8), cliente));
        if (Objects.nonNull(clienteDto.getAltriDocumenti())) clienteDto.setAltriDocumenti(addDocumentoTypeCliente(clienteDto.getAltriDocumenti(),setTipoDocumento(9), cliente));
        Integer idClientePratica = addClientePratica(idPratica, cliente, clienteDto.getIdTipCliente(), checkStato(clienteDto));
        clienteDto.setIdClientePratica(idClientePratica);
        cliente = clienteRepository.save(cliente);
        return ResponseEntity.ok(clienteDto);
    }
    @PostMapping(value = "deleteClienteById/{idPratica}/{idCliente}/{idType}")
    public void deleteClienteById(@PathVariable Integer idPratica, @PathVariable Integer idCliente, @PathVariable Integer idType) {
        Optional<Cliente> cliente = clienteRepository.findById(Integer.valueOf(idCliente));
        if (cliente.isPresent()) {
            List<DocumentoCliente> listDocCliente = documentoClienteRepository.findDocumentoClienteByIdCliente(cliente.get());
            for(DocumentoCliente dc: listDocCliente){
                documentoClienteRepository.deleteById(dc.getId());
            }
            Optional<ClientePratica> clientePratica = clientePraticaRepository.findByIdClienteAndIdPraticaAndIdTipCliente(idCliente, idPratica, idType);
            if (cliente.isPresent()) {
                clientePraticaRepository.deleteById(clientePratica.get().getId());
            }
            clienteRepository.deleteById(idCliente);
        }
    }

    @PostMapping(value = "deleteDocumentoById/{idDocumento}")
    public ResponseEntity<?> deleteDocumento(@PathVariable Integer idDocumento) {
        documentoClienteRepository.deleteById(idDocumento);
        return ResponseEntity.ok().build();
    }

    private Integer addClientePratica(String idPratica, Cliente cliente, TipCliente tipCliente, Integer stato) {
        ClientePratica clientePratica;
        Optional<ClientePratica> optClientePratica = clientePraticaRepository.findByIdClienteAndIdPraticaAndIdTipCliente(cliente.getId(), Integer.valueOf(idPratica), tipCliente.getId());
        if (!optClientePratica.isPresent()) {
            clientePratica = new ClientePratica();
            clientePratica.setIdCliente(cliente.getId());
            clientePratica.setIdPratica(Integer.valueOf(idPratica));
            clientePratica.setIdTipCliente(tipCliente.getId());
        } else {
            clientePratica = optClientePratica.get();
            clientePratica.setStato(stato);
        }
        clientePratica = clientePraticaRepository.save(clientePratica);
            return clientePratica.getId();
    }

    private TipDocumento setTipoDocumento(Integer id) {
        TipDocumento tipDocumento = new TipDocumento();
        tipDocumento.setId(id);
        return tipDocumento;
    }

    private List<DocumentoDto> addDocumentoTypeCliente(List<DocumentoDto> docList, TipDocumento tipDocumento, Cliente cliente) {
        List<DocumentoDto> listDocDto = new ArrayList();
        if (!CollectionUtils.isEmpty(docList)) {
            for (DocumentoDto documentoDto : docList) {
                Optional<DocumentoCliente> optdocumento = documentoClienteRepository.findByIdClienteAndIdTipDocumentoAndFilename(cliente, tipDocumento, documentoDto.getFilename());
                if (!optdocumento.isPresent()) {
                    DocumentoCliente documentoCliente = convertDocumentoDto(documentoDto);
                    documentoCliente.setIdTipDocumento(tipDocumento);
                    documentoCliente.setIdCliente(cliente);
                    documentoCliente = documentoClienteRepository.save(documentoCliente);
                    listDocDto.add(convertDocumentoCliente(documentoCliente));
                } else {
                    listDocDto.add(documentoDto);
                }
            }
        }
        return listDocDto;
    }
    private DocumentoDto convertDocumentoCliente(DocumentoCliente documentoCliente) {
        DocumentoDto documentoDto = new DocumentoDto();
        documentoDto.setFilename(documentoCliente.getFilename());
        documentoDto.setId(documentoCliente.getId());
        documentoDto.setPathDoc(documentoCliente.getPathDoc());
        documentoDto.setTipoDoc(documentoCliente.getTipoDoc());
        return documentoDto;
    }
    private DocumentoCliente convertDocumentoDto(DocumentoDto documentoDto) {
        DocumentoCliente documentoCliente = new DocumentoCliente();
        documentoCliente.setFilename(documentoDto.getFilename());
        documentoCliente.setId(documentoDto.getId());
        documentoCliente.setPathDoc(documentoDto.getPathDoc());
        documentoCliente.setTipoDoc(documentoDto.getTipoDoc());
        return documentoCliente;
    }

    private Cliente convertCliente(ClienteDto clienteDto) {
        Cliente cliente = new Cliente();
        if (Objects.nonNull(clienteDto.getId())) cliente.setId(clienteDto.getId());
        if (Objects.nonNull(clienteDto.getIdTipCliente())) cliente.setIdTipCliente(clienteDto.getIdTipCliente());
        if (Objects.nonNull(clienteDto.getNome())) cliente.setNome(clienteDto.getNome());
        if (Objects.nonNull(clienteDto.getCognome())) cliente.setCognome(clienteDto.getCognome());
        if (Objects.nonNull(clienteDto.getRagioneSociale())) cliente.setRagioneSociale(clienteDto.getRagioneSociale());
        if (Objects.nonNull(clienteDto.getPiva())) cliente.setPiva(clienteDto.getPiva());
        if (Objects.nonNull(clienteDto.getPec())) cliente.setPec(clienteDto.getPec());
        if (Objects.nonNull(clienteDto.getCf())) cliente.setCf(clienteDto.getCf());
        if (Objects.nonNull(clienteDto.getIndirizzo())) cliente.setIndirizzo(clienteDto.getIndirizzo());
        if (Objects.nonNull(clienteDto.getCivico())) cliente.setCivico(clienteDto.getCivico());
        if (Objects.nonNull(clienteDto.getCitta())) cliente.setCitta(clienteDto.getCitta());
        if (Objects.nonNull(clienteDto.getProvincia())) cliente.setProvincia(clienteDto.getProvincia());
        if (Objects.nonNull(clienteDto.getCap())) cliente.setCap(clienteDto.getCap());
        return cliente;
    }

    private Integer checkStato(ClienteDto clienteDto) {
        if ((Objects.isNull(clienteDto.getNome()) || Objects.isNull(clienteDto.getCognome())) && Objects.isNull(clienteDto.getRagioneSociale())) return 0;
        if (Objects.isNull(clienteDto.getPec())) return 0;
        if (Objects.isNull(clienteDto.getCf())) return 0;
        if (Objects.isNull(clienteDto.getIndirizzo())) return 0;;
        if (Objects.isNull(clienteDto.getCivico())) return 0;
        if (Objects.isNull(clienteDto.getCitta())) return 0;
        if (Objects.isNull(clienteDto.getProvincia())) return 0;
        if (Objects.isNull(clienteDto.getCap())) return 0;

        // CHECK STATO PERSONA
        if ((Objects.nonNull(clienteDto.getNome()) && Objects.nonNull(clienteDto.getCognome()))){
            if (CollectionUtils.isEmpty(clienteDto.getDocumentoIdentita())) return 0;
            if (CollectionUtils.isEmpty(clienteDto.getDichiarazioneRedditi())) return 0;
            if (CollectionUtils.isEmpty(clienteDto.getVisuraCatastale())) return 0;
        }
        // CHECK STATO SOCIETA
        if (Objects.nonNull(clienteDto.getRagioneSociale())){
            if (Objects.isNull(clienteDto.getPiva())) return 0;
            if (CollectionUtils.isEmpty(clienteDto.getDocumentoIdentitaLegaleRappresentante())) return 0;
            if (CollectionUtils.isEmpty(clienteDto.getBilancioCompletoUltimoEsercizioChiuso())) return 0;
            if (CollectionUtils.isEmpty(clienteDto.getBilancioProvvisorioEsercizioCorso())) return 0;
            if (CollectionUtils.isEmpty(clienteDto.getVisuraCamerale())) return 0;
        }

        return 1;
    }

}
