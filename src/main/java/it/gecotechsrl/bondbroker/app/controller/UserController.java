package it.gecotechsrl.bondbroker.app.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import it.gecotechsrl.bondbroker.dto.NewUserDto;
import it.gecotechsrl.bondbroker.dto.UserAppReq;
import it.gecotechsrl.bondbroker.dto.UserDetailReq;
import it.gecotechsrl.bondbroker.jwt.dao.AuthorityRepository;
import it.gecotechsrl.bondbroker.jwt.dao.UserRepository;
import it.gecotechsrl.bondbroker.jwt.model.AuthorityName;
import it.gecotechsrl.bondbroker.jwt.model.User;
import it.gecotechsrl.bondbroker.jwt.model.UsersDetail;
import it.gecotechsrl.bondbroker.jwt.security.JwtTokenUtil;
import it.gecotechsrl.bondbroker.jwt.service.JwtUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("protected/user/")
public class UserController {
    Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @PostMapping(value = "userById")
    public ResponseEntity getUserById(@RequestBody UserAppReq userReq) {
        Optional<User> user = userRepository.findById(userReq.getId());
        return user.isPresent() ? ResponseEntity.ok(user.get()) : ResponseEntity.badRequest().body("USER NOT EXIST");
    }


    @PostMapping(value = "userByUID")
    public ResponseEntity getUserDetail(@RequestBody UserAppReq userReq) {
        Optional<User> user = userRepository.findByUsername(userReq.getUid());
        return user.isPresent() ? ResponseEntity.ok(user.get()) : ResponseEntity.badRequest().body("USER NOT EXIST");
    }

    @PostMapping(value = "addNewUser")
    @Transactional
    public ResponseEntity addNewUser(@RequestBody UserAppReq userAppReq) {
        UserRecord userRecord = null;
        try {
            UserRecord.CreateRequest createRequest = new UserRecord.CreateRequest();
            createRequest.setEmail(userAppReq.getMail());
            createRequest.setPassword(userAppReq.getPassword());
            if (!Objects.isNull(userAppReq.getName()) && !Objects.isNull(userAppReq.getSurname()))
                createRequest.setDisplayName(userAppReq.getName() + " " + userAppReq.getSurname());
            if (!Objects.isNull(userAppReq.getPhone()))
                createRequest.setPhoneNumber(userAppReq.getPhone());
            userRecord = FirebaseAuth.getInstance().createUser(createRequest);
            User user = addUser(userRecord.getUid(), userAppReq);
            String emailVerification = FirebaseAuth.getInstance().generateEmailVerificationLink(userRecord.getEmail());
            NewUserDto newUserDto = new NewUserDto();
            newUserDto.setId(user.getId());
            newUserDto.setNome(user.getUsersDetail().getName());
            newUserDto.setCognome(user.getUsersDetail().getSurname());
            newUserDto.setLink(emailVerification);
            // ParamsTemplate paramsTemplate = new ParamsTemplate(emailVerification, user.getUsersDetail().getName(), user.getUsersDetail().getSurname(), userRecord.getEmail(), LocalDate.now().toString());
            // asyncSendEmailSms.sendRegistrationMailMessage(paramsTemplate);
            return ResponseEntity.ok(newUserDto);
        } catch (FirebaseAuthException e) {
            String errorMsg = e.getMessage() + "-" + e.getErrorCode();
            logger.error("FirebaseAuthException {}", errorMsg);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMsg);
        } catch (DisabledException disabledException) {
            logger.error("disabledException Exception", disabledException);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } catch (Exception e) {
            logger.error("Generic Exception", e);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private User addUser(String uid, UserAppReq userReq) {
        User user = new User();
        user.setEnabled(true);
        user.setUsername(uid);
        user.setPassword(passwordEncoder.encode(uid));
        if (Objects.nonNull(userReq.getAuthorityName()))
            user.addAuthority(authorityRepository.findByName(AuthorityName.valueOf(userReq.getAuthorityName())));
        else
            user.addAuthority(authorityRepository.findByName(AuthorityName.ROLE_USER));
        UsersDetail usersDetail = new UsersDetail();
        if (Objects.nonNull(userReq.getName())) usersDetail.setName(userReq.getName());
        if (Objects.nonNull(userReq.getSurname())) usersDetail.setSurname(userReq.getSurname());
        if (Objects.nonNull(userReq.getBirthDate())) usersDetail.setDataNascita(userReq.getBirthDate());
        if (Objects.nonNull(userReq.getCf())) usersDetail.setCf(userReq.getCf());
        if (Objects.nonNull(userReq.getMail())) usersDetail.setMail(userReq.getMail());
        if (Objects.nonNull(userReq.getPhone())) usersDetail.setPhone(userReq.getPhone());
        if (Objects.nonNull(userReq.getIdAdminOwner())) usersDetail.setAdminUserId(userReq.getIdAdminOwner());
        usersDetail.setPrimoAccesso(true);

        usersDetail.setUser(user);
        user.setUsersDetail(usersDetail);
        user = userRepository.save(user);
        return user;
    }

    @PostMapping(value = "modifyUser")
    @Transactional
    public ResponseEntity modifyUserDetail(@RequestBody UserDetailReq userReq) {
        try {
            Optional<User> userOptional = userRepository.findById(userReq.getId());
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                setUserDetail(userReq, user);
                user = userRepository.save(user);
                return ResponseEntity.ok(user);
            } else {
                logger.error("Modify USER DETAIL USER ID {} UNDEFINED", userReq.getId());
                return ResponseEntity.badRequest().body("USER ID UNDEFINED");
            }
        } catch (Exception e) {
            logger.error("Modify USER DETAIL {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("ERROR ADD USER DETAIL");
        }
    }

    @PostMapping(value = "modifyReviewUser")
    @Transactional
    public ResponseEntity modifyReviewUser(@RequestBody UserDetailReq userReq) {
        try {
            Optional<User> userOptional = userRepository.findById(userReq.getId());
            if (userOptional.isPresent()) {
                User user = userOptional.isPresent() ? userOptional.get() : new User();
                user = userRepository.save(user);
                return ResponseEntity.ok(user);
            }
            logger.error("Modify USER DETAIL USER ID {} UNDEFINED", userReq.getId());
            return ResponseEntity.badRequest().body("USER ID UNDEFINED");
        } catch (Exception e) {
            logger.error("Modify USER DETAIL {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("ERROR ADD USER DETAIL");
        }
    }

    private void setUserDetail(UserDetailReq userReq, User user) {
        UsersDetail userD = user.getUsersDetail();
        if (Objects.nonNull(userReq.getName())) userD.setName(userReq.getName());
        if (Objects.nonNull(userReq.getSurname())) userD.setSurname(userReq.getSurname());
        if (Objects.nonNull(userReq.getImage())) userD.setImage(userReq.getImage());
        if (Objects.nonNull(userReq.getDataNascita())) userD.setDataNascita(userReq.getDataNascita());
        if (Objects.nonNull(userReq.getTokenMobile())) userD.setTokenMobile(userReq.getTokenMobile());
        if (Objects.nonNull(userReq.getMail())) userD.setMail(userReq.getMail());
        if (Objects.nonNull(userReq.getCf())) userD.setCf(userReq.getCf());
        if (Objects.nonNull(userReq.getPhone())) userD.setPhone(userReq.getPhone());
        userD.setPrimoAccesso(userReq.isPrimoAccesso());
        user.setEnabled(userReq.isEnabled());
        if (Objects.nonNull(userReq.getAuthorityName())) {
            if (user.getAuthorities().isEmpty()) {
                user.addAuthority(authorityRepository.findByName(AuthorityName.valueOf(userReq.getAuthorityName())));
            }
            if (!user.getAuthorities().isEmpty() && !user.getAuthorities().get(0).getName().name().equals(userReq.getAuthorityName())) {
                user.removeAuthority(authorityRepository.getOne(user.getAuthorities().get(0).getId()));
                user.addAuthority(authorityRepository.findByName(AuthorityName.valueOf(userReq.getAuthorityName())));
            }
        }
        /*if (Objects.nonNull(user.getUsersDetail()) && Objects.nonNull(user.getUsersDetail().getTokenMobile())) {
            List<String> tokenMobile = new ArrayList<>();
            tokenMobile.add(user.getUsersDetail().getTokenMobile());
            if (!userReq.isEnabled()) {
                try {
                    TopicManagementResponse response = FirebaseMessaging.getInstance().unsubscribeFromTopic(
                            tokenMobile, "AGENZIA" + user.getUsersDetail().getAgenzia().getId());
                    userD.setTokenMobile(null);
                } catch (FirebaseMessagingException fb) {
//                    logger.info("FirebaseMessagingException: " + fb.getMessage());
                }
            }
        }*/
    }

    @GetMapping(value = "getUserById/{idUser}")
    public ResponseEntity<?> getUserById(@PathVariable String idUser) {
        try {
            Optional<User> user = userRepository.findById(Integer.valueOf(idUser));
            return ResponseEntity.ok(user.isPresent() ? user.get() : new User());
        } catch (Exception e) {
            logger.error("GET USER BY ID : {}", e.getMessage(), e);
            return ResponseEntity.badRequest().body("USER NOT FIND !!!");
        }
    }
    @GetMapping(value = "getListUser/{idUser}")
    public ResponseEntity<?> getListUser(@PathVariable String idUser) {
        try {
            List<User> user = userRepository.findByUsersDetail_AdminUserId(Integer.valueOf(idUser));
            return ResponseEntity.ok(Objects.nonNull(user) ? user : new ArrayList());
        } catch (Exception e) {
            logger.error("GET USER BY ID : {}", e.getMessage(), e);
            return ResponseEntity.badRequest().body("USER NOT FIND !!!");
        }
    }
    @GetMapping("getListUserControl")
    public ResponseEntity<?> getListUserControl() {
        return ResponseEntity.ok(userRepository.findByAuthorities_id(3));
    }

}


