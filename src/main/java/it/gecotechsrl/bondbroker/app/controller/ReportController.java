package it.gecotechsrl.bondbroker.app.controller;

import it.gecotechsrl.bondbroker.app.async.AsyncGetReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

@RestController
public class ReportController {
    @Autowired
    AsyncGetReport asyncGetReport;

    @GetMapping(value = "public/getPdfPratica/{idPratica}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<?> getPdfPratica(HttpServletRequest request,  @PathVariable Integer idPratica) throws IOException, ExecutionException, InterruptedException {

        LocalDateTime localDateTime = LocalDateTime.now();
        String dataTime= localDateTime.format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss"));
         ByteArrayOutputStream os = (ByteArrayOutputStream) asyncGetReport.getPdfPratica(idPratica).get();
        byte[] zippedData = os.toByteArray();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentDisposition(ContentDisposition.builder("attachment").filename("ModuloRichiestaGaranzia_" + dataTime + ".pdf").build());
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        httpHeaders.setContentLength(zippedData.length);
        return ResponseEntity.ok().headers(httpHeaders).body(zippedData);
    }
    @GetMapping("public/getZipPratica/{idPratica}/{uid}")
    public void getZipPratica(HttpServletResponse response, @PathVariable Integer idPratica, @PathVariable String uid) throws IOException, ExecutionException, InterruptedException {
        LocalDateTime localDateTime = LocalDateTime.now();
        String dataTime= localDateTime.format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss"));
        response.setHeader(
                HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"pratica_" + dataTime + ".zip\"");
        asyncGetReport.getZipPratica(response.getOutputStream(), idPratica, uid).get().flush();
    }

}
