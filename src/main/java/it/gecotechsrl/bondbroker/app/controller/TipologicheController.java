package it.gecotechsrl.bondbroker.app.controller;

import it.gecotechsrl.bondbroker.app.dao.TipClienteRepository;
import it.gecotechsrl.bondbroker.app.dao.TipFirmaRepository;
import it.gecotechsrl.bondbroker.app.dao.TipGaranziaRepository;
import it.gecotechsrl.bondbroker.app.dao.TipStatoRepository;
import it.gecotechsrl.bondbroker.app.model.Pratica;
import it.gecotechsrl.bondbroker.dto.PraticaDto;
import it.gecotechsrl.bondbroker.jwt.dao.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("protected/tipologica/")
public class TipologicheController {
    Logger logger = LoggerFactory.getLogger(TipologicheController.class);
    @Autowired
    private TipClienteRepository tipClienteRepository;
    @Autowired
    private TipGaranziaRepository tipGaranziaRepository;
    @Autowired
    private TipFirmaRepository tipFirmaRepository;
    @Autowired
    private TipStatoRepository tipStatoRepository;

    @GetMapping(value = "getTipGaranzia")
    public ResponseEntity<?> getTipGaranzia() {
        return ResponseEntity.ok(tipGaranziaRepository.findAll());
    }
    @GetMapping(value = "getTipFirma")
    public ResponseEntity<?> getTipFirma() {
        return ResponseEntity.ok(tipFirmaRepository.findAll());
    }
    @GetMapping(value = "getTipStato")
    public ResponseEntity<?> getTipStato() {
        return ResponseEntity.ok(tipStatoRepository.findAll());
    }

}
