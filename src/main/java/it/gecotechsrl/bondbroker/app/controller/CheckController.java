package it.gecotechsrl.bondbroker.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckController {

    @GetMapping(value = "public/checkService")
    public ResponseEntity checkService() {
        return ResponseEntity.ok("service ready");
    }


}
