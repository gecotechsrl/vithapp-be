package it.gecotechsrl.bondbroker.app.controller;

import it.gecotechsrl.bondbroker.Constants;
import it.gecotechsrl.bondbroker.app.dao.*;
import it.gecotechsrl.bondbroker.app.model.*;
import it.gecotechsrl.bondbroker.app.utils.ConverterUtils;
import it.gecotechsrl.bondbroker.dto.*;
import it.gecotechsrl.bondbroker.jwt.dao.UserRepository;
import it.gecotechsrl.bondbroker.jwt.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("protected/pratica/")
public class PraticaController {
    Logger logger = LoggerFactory.getLogger(PraticaController.class);
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PraticaRepository praticaRepository;
    @Autowired
    private ClientePraticaRepository clientePraticaRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private DocumentoPraticaRepository documentoPraticaRepository;
    @Autowired
    private DocumentoClienteRepository documentoClienteRepository;
    @Autowired
    private ConverterUtils converterUtils;


    @GetMapping(value = "getPraticaById/{idPratica}")
    public ResponseEntity<?> getPraticaById(@PathVariable String idPratica) {
        PraticaDto praticaDto = converterUtils.getPraticaDto(Integer.valueOf(idPratica));
        return Objects.nonNull(praticaDto) ? ResponseEntity.ok(praticaDto) : ResponseEntity.badRequest().body("PRATICA NOT FIND !!!");
    }

    @PostMapping(value = "deletePraticaById/{idPratica}")
    @Transactional
    public ResponseEntity<?> deletePraticaById(@PathVariable String idPratica) {
        Optional<Pratica> praticaOptional = praticaRepository.findById(Integer.valueOf(idPratica));
        if (praticaOptional.isPresent()) {
            Pratica pratica = praticaOptional.get();
            // RITROVA DOCUMENTO_PRATICA
            List<DocumentoPratica> documentoPraticaList = documentoPraticaRepository.findDocumentoPraticaByIdPratica(pratica.getId());
            if (!CollectionUtils.isEmpty(documentoPraticaList)) {
                for (DocumentoPratica documentoPratica : documentoPraticaList) {
                    documentoPraticaRepository.deleteById(documentoPratica.getId());
                }
            }
            // CLIENTE PRATICA
            List<ClientePratica> clientePraticaList = clientePraticaRepository.findAllByIdPratica(pratica.getId());
            if (!CollectionUtils.isEmpty(clientePraticaList)) {
                for (ClientePratica clientePratica : clientePraticaList) {
                    List<DocumentoCliente> listDocCliente = documentoClienteRepository.findDocumentoClienteByIdCliente_Id(clientePratica.getIdCliente());
                    for (DocumentoCliente dc : listDocCliente) {
                        documentoClienteRepository.deleteById(dc.getId());
                    }
                    clientePraticaRepository.deleteById(clientePratica.getId());
                    clienteRepository.deleteById(clientePratica.getIdCliente());
                }
            }
            praticaRepository.deleteById(pratica.getId());
        }
        return ResponseEntity.ok().build();
    }

    private DocumentoPratica convertDocumentoDto(DocumentoDto documentoDto) {
        DocumentoPratica documentoPratica = new DocumentoPratica();
        documentoPratica.setFilename(documentoDto.getFilename());
        documentoPratica.setId(documentoDto.getId());
        documentoPratica.setPathDoc(documentoDto.getPathDoc());
        documentoPratica.setTipoDoc(documentoDto.getTipoDoc());
        return documentoPratica;
    }

    @PostMapping(value = "listPraticheById")
    public ResponseEntity<?> listPraticheById(@RequestBody FindPraticaDto findPraticaDto) {
        Sort.Order order = new Sort.Order(findPraticaDto.getDirection().equals("desc") ? Sort.Direction.DESC : Sort.Direction.ASC, Objects.nonNull(findPraticaDto.getColumn()) ? findPraticaDto.getColumn() : "id").ignoreCase();
        Pageable pageable = PageRequest.of(findPraticaDto.getPage(), findPraticaDto.getSize(), Sort.by(order));
        List<PraticaDto> praticaDtoList;
        try {
            List<Integer> listUserId = userRepository.getListUserIdByAdminUserId(findPraticaDto.getIdUser());
            listUserId = !CollectionUtils.isEmpty(listUserId) ? listUserId : Arrays.asList(findPraticaDto.getIdUser());
            Page<Pratica> praticaList = praticaRepository.findAllByOwner_IdIn(listUserId, pageable);
            if (!CollectionUtils.isEmpty(praticaList.getContent())) {
                praticaDtoList = new ArrayList();
                for (Pratica pratica : praticaList.getContent()) {
                    // CLIENTI
                    List<ClientePratica> clientiPraticaList = clientePraticaRepository.findAllByIdPratica(pratica.getId());
                    praticaDtoList.add(convertPraticaLight(pratica, clientiPraticaList));
                }
                PageObject praticaPageObject = new PageObject(praticaList.getNumber() + 1 < praticaList.getTotalPages(), praticaList.getNumber(), praticaList.getTotalPages(), praticaList.getTotalElements(), praticaDtoList);
                return ResponseEntity.ok(praticaPageObject);
            }
        } catch (Exception e) {
            logger.error("GET USER BY ID : {}", e.getMessage(), e);
        }
        PageObject praticaPageObject = new PageObject(false, findPraticaDto.getPage(), 0, 0, new ArrayList());
        return ResponseEntity.ok(praticaPageObject);
    }

    private PraticaDto convertPraticaLight(Pratica pratica, List<ClientePratica> clientiPraticaList) {
        PraticaDto praticaDto = converterUtils.convertPraticaTable(pratica);
        String contraente = Constants.EMPTY_STRING;
        String initBeneficiario = Constants.BENEFICIARIO_STRING + Constants.WHITE_SPACE_STRING;
        String beneficiario = initBeneficiario;
        for (ClientePratica clientePratica : clientiPraticaList) {
            if (clientePratica.getIdTipCliente().intValue() == 1) {
                contraente = contraente.concat(getIntestazioneCliente(clienteRepository.findById(clientePratica.getIdCliente()).get())).concat(" -").concat(Constants.WHITE_SPACE_STRING);
            }
            if (clientePratica.getIdTipCliente().intValue() == 2) {
                beneficiario = beneficiario.concat(getIntestazioneCliente(clienteRepository.findById(clientePratica.getIdCliente()).get())).concat(" -").concat(Constants.WHITE_SPACE_STRING);
            }
        }
        String contraenteTxt = contraente.concat(beneficiario);
        contraenteTxt = contraenteTxt.lastIndexOf("-") > 0 ? contraenteTxt.substring(0, contraenteTxt.lastIndexOf("-")) : contraenteTxt;
        praticaDto.setContraenteTxt(contraenteTxt.equals(initBeneficiario) ? Constants.EMPTY_STRING : contraenteTxt);
        praticaDto.setOwnerName(getIntestazioneOwner(pratica.getOwner()));
        return praticaDto;
    }

    private String getIntestazioneCliente(Cliente cliente) {
        if (Objects.nonNull(cliente.getNome()))
            return cliente.getNome().concat(Constants.WHITE_SPACE_STRING).concat(cliente.getCognome());
        if (Objects.nonNull(cliente.getRagioneSociale()))
            return cliente.getRagioneSociale();
        return Constants.EMPTY_STRING;
    }

    private String getIntestazioneOwner(User user) {
        if (Objects.nonNull(user.getUsersDetail()))
            return Objects.nonNull(user.getUsersDetail().getName())
                    ? user.getUsersDetail().getName().concat(Constants.WHITE_SPACE_STRING).concat(
                    Objects.nonNull(user.getUsersDetail().getSurname()) ? user.getUsersDetail().getSurname() : Constants.EMPTY_STRING)
                    : Objects.nonNull(user.getUsersDetail().getSurname()) ? user.getUsersDetail().getSurname() : Constants.EMPTY_STRING;

        return Constants.EMPTY_STRING;
    }


    @PostMapping(value = "createNewPratica/{idOwner}")
    public ResponseEntity<?> createNewPratica(@PathVariable Integer idOwner) {
        PraticaDto praticaDto = new PraticaDto();
        Pratica pratica = new Pratica();
        User user = new User();
        user.setId(idOwner);
        pratica.setOwner(user);
        praticaDto.setId(praticaRepository.save(pratica).getId());
        return ResponseEntity.ok(praticaDto);
    }

    @PostMapping(value = "addUpdatePratica")
    @Transactional
    public ResponseEntity<?> addUpdatePratica(@RequestBody PraticaDto praticaDto) {
        Pratica pratica = convertPraticaDto(praticaDto);
        pratica.setStato(checkStato(praticaDto));
        pratica.setDataModifica(LocalDateTime.now());
        pratica = praticaRepository.save(pratica);
        if (Objects.nonNull(praticaDto.getDocumentazioneTecnica())) {
            List<DocumentoDto> docTecnicaDto = new ArrayList();
            for (DocumentoDto documentoDto : praticaDto.getDocumentazioneTecnica()) {
                Optional<DocumentoPratica> optdocumento = documentoPraticaRepository.findByIdPraticaAndFilename(pratica.getId(), documentoDto.getFilename());
                if (!optdocumento.isPresent()) {
                    TipDocumento tipDocumento = new TipDocumento();
                    tipDocumento.setId(1);
                    DocumentoPratica documentoPratica = convertDocumentoDto(documentoDto);
                    documentoPratica.setIdPratica(pratica.getId());
                    documentoPratica.setIdTipDocumento(tipDocumento);
                    documentoPratica = documentoPraticaRepository.save(documentoPratica);
                    docTecnicaDto.add(converterUtils.convertDocumentoPratica(documentoPratica));
                } else {
                    docTecnicaDto.add(documentoDto);
                }
            }
            praticaDto.setDocumentazioneTecnica(docTecnicaDto);
        }
        return ResponseEntity.ok(praticaDto);
    }

    private Integer checkStato(PraticaDto praticaDto) {
        if (CollectionUtils.isEmpty(praticaDto.getContraente())) {
            return 0;
        } else {
            for (ClienteDto clienteDto : praticaDto.getContraente())
                if (clienteDto.getStato().intValue() == 0) return 0;
        }
        if (CollectionUtils.isEmpty(praticaDto.getBeneficiario())) {
            return 0;
        } else {
            for (ClienteDto clienteDto : praticaDto.getBeneficiario())
                if (clienteDto.getStato().intValue() == 0) return 0;
        }
        if (CollectionUtils.isEmpty(praticaDto.getDocumentazioneTecnica())) return 0;
        if (Objects.isNull(praticaDto.getOggetto())) return 0;
        if (Objects.isNull(praticaDto.getIdTipGaranzia())) return 0;
        if (Objects.isNull(praticaDto.getValidoDa())) return 0;
        if (Objects.isNull(praticaDto.getValidoA())) return 0;
        if (Objects.isNull(praticaDto.getImporto())) return 0;
        if (Objects.isNull(praticaDto.getTasso())) return 0;
        if (Objects.isNull(praticaDto.getPremio())) return 0;
        if (Objects.isNull(praticaDto.getIdTipFirma())) return 0;
        return 1;
    }

    private Pratica convertPraticaDto(PraticaDto praticaDto) {
        Pratica pratica = new Pratica();
        if (Objects.nonNull(praticaDto.getId())) pratica.setId(praticaDto.getId());
        if (Objects.nonNull(praticaDto.getIdTipGaranzia())) pratica.setIdTipGaranzia(praticaDto.getIdTipGaranzia());
        if (Objects.nonNull(praticaDto.getOggetto())) pratica.setOggetto(praticaDto.getOggetto());
        if (Objects.nonNull(praticaDto.getNote())) pratica.setNote(praticaDto.getNote());
        if (Objects.nonNull(praticaDto.getValidoDa())) pratica.setValidoDa(praticaDto.getValidoDa());
        if (Objects.nonNull(praticaDto.getValidoA())) pratica.setValidoA(praticaDto.getValidoA());
        if (Objects.nonNull(praticaDto.getDurataVal())) pratica.setDurataVal(praticaDto.getDurataVal());
        if (Objects.nonNull(praticaDto.getExtGaranziaDa())) pratica.setExtGaranziaDa(praticaDto.getExtGaranziaDa());
        if (Objects.nonNull(praticaDto.getExtGaranziaA())) pratica.setExtGaranziaA(praticaDto.getExtGaranziaA());
        if (Objects.nonNull(praticaDto.getDurataExt())) pratica.setDurataExt(praticaDto.getDurataExt());
        if (Objects.nonNull(praticaDto.getImporto())) pratica.setImporto(praticaDto.getImporto());
        if (Objects.nonNull(praticaDto.getTasso())) pratica.setTasso(praticaDto.getTasso());
        if (Objects.nonNull(praticaDto.getPremio())) pratica.setPremio(praticaDto.getPremio());
        if (Objects.nonNull(praticaDto.getValoreProgetto())) pratica.setValoreProgetto(praticaDto.getValoreProgetto());
        if (Objects.nonNull(praticaDto.getIdTipFirma())) pratica.setIdTipFirma(praticaDto.getIdTipFirma());
        if (Objects.nonNull(praticaDto.getIdTipStato())) pratica.setIdTipStato(praticaDto.getIdTipStato());
        if (Objects.nonNull(praticaDto.getOwner())) pratica.setOwner(praticaDto.getOwner());
        if (Objects.nonNull(praticaDto.getStato())) pratica.setStato(praticaDto.getStato());
        if (Objects.nonNull(praticaDto.getDataModifica())) pratica.setDataModifica(praticaDto.getDataModifica());
        return pratica;
    }

    @PostMapping(value = "deleteDocumentoById/{idDocumento}")
    public ResponseEntity<?> deleteDocumento(@PathVariable Integer idDocumento) {
        documentoPraticaRepository.deleteById(idDocumento);
        return ResponseEntity.ok().build();
    }


}


