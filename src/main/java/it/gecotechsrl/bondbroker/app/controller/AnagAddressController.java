package it.gecotechsrl.bondbroker.app.controller;

import it.gecotechsrl.bondbroker.app.dao.CitiesRepository;
import it.gecotechsrl.bondbroker.app.dao.ProvinciesRepository;
import it.gecotechsrl.bondbroker.app.model.ComuniIntefarce;
import it.gecotechsrl.bondbroker.app.model.Provincies;
import it.gecotechsrl.bondbroker.dto.ComuniDto;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("protected/anagAddress/")
public class AnagAddressController {
    private static final Logger log = LoggerFactory.getLogger(AnagAddressController.class);
    @Autowired
    private CitiesRepository citiesRepository;
    @Autowired
    private ProvinciesRepository provinciesRepository;



    @GetMapping(value = "getAllProv")
    public ResponseEntity<?> getAllProv() {
        List<Provincies> provinciesList = provinciesRepository.findAll();
        return ResponseEntity.ok(provinciesList);
    }

    @GetMapping(value = "getAllComuni/{siglaProv}")
    public ResponseEntity<?> getAllComuni(@PathVariable String siglaProv) {
        List<ComuniDto> comuniDtoList = new ArrayList<>();
        try {
            List<ComuniIntefarce> comuniList = citiesRepository.findAllComuniByCodProv(siglaProv);
            for (ComuniIntefarce comuniIntefarce : comuniList) {
                ComuniDto comuniDto = new ComuniDto();
                comuniDto.setIstat(comuniIntefarce.getIstat());
                comuniDto.setComune(comuniIntefarce.getComune());
                comuniDto.setSiglaProvincia(comuniIntefarce.getProvincia());
                String capString = comuniIntefarce.getCap();
                List<String> capList = new ArrayList();
                if (capString.contains("-")) {
                    int indexMinus = capString.indexOf("-");
                    Integer fromCap = Integer.valueOf(capString.substring(0, indexMinus));
                    Integer toCap = Integer.valueOf(capString.substring(indexMinus + 1));
                    for (int i = fromCap.intValue(); i <= toCap.intValue(); i++) {
                        capList.add(StringUtils.leftPad(String.valueOf(i), 5, "0"));
                    }
                } else {
                    capList.add(capString);
                }
                comuniDto.setCap(capList);
                comuniDtoList.add(comuniDto);
            }
        } catch (Exception e) {
            log.error("GET ALL COMUNI : {}", e.getMessage(), e);
            return ResponseEntity.badRequest().body("GET ALL COMUNI !!!");
        }
        return ResponseEntity.ok(comuniDtoList);
    }
}
