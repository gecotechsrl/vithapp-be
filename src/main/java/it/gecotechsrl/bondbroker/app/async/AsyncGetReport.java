package it.gecotechsrl.bondbroker.app.async;

import it.gecotechsrl.bondbroker.app.pdf.PdfDocument;
import it.gecotechsrl.bondbroker.app.utils.ConverterUtils;
import it.gecotechsrl.bondbroker.dto.ClienteDto;
import it.gecotechsrl.bondbroker.dto.DocumentoDto;
import it.gecotechsrl.bondbroker.dto.PraticaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
public class AsyncGetReport {
    Logger log = LoggerFactory.getLogger(AsyncGetReport.class);

    @Autowired
    ConverterUtils converterUtils;

    @Async("asyncExecutor")
    public CompletableFuture<OutputStream> getPdfPratica(Integer idPratica) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PraticaDto praticaDto = converterUtils.getPraticaDto(idPratica);
        if (Objects.nonNull(praticaDto)) {
            PdfDocument pdfDocument = new PdfDocument(os);
            pdfDocument.creaPdf(praticaDto);
        }
        return CompletableFuture.completedFuture(os);
    }

    @Async("asyncExecutor")
    public CompletableFuture<OutputStream> getZipPratica(OutputStream os, Integer idPratica, String uid) {


        try {
            PraticaDto praticaDto = converterUtils.getPraticaDto(idPratica);
            if (Objects.nonNull(praticaDto)) {
                byte[] buffer = new byte[1024];
                ZipOutputStream zos = new ZipOutputStream(os);
                ByteArrayOutputStream osBozza = new ByteArrayOutputStream();
                PdfDocument pdfDocument = new PdfDocument(osBozza);
                pdfDocument.creaPdf(praticaDto);
                InputStream is = new ByteArrayInputStream(osBozza.toByteArray());
                zos.putNextEntry(new ZipEntry("MODULO RICHIESTA GARANZIA.pdf"));
                int length;
                while ((length = is.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }
                zos.closeEntry();
                is.close();
                osBozza.close();
                addFileFolder(zos, praticaDto.getDocumentazioneTecnica(), "Doc_tecnica/");
                addClienteFileFolder(zos, praticaDto.getContraente());
                addClienteFileFolder(zos, praticaDto.getBeneficiario());
                addClienteFileFolder(zos, praticaDto.getCoobbligato());
                zos.close();
            }
        } catch (Exception e) {
            log.error("ZIP ERROR : {}", e.getMessage());
        }
        return CompletableFuture.completedFuture(os);
    }

    private void addClienteFileFolder(ZipOutputStream zos, List<ClienteDto> listCliente) throws IOException {
        if (!CollectionUtils.isEmpty(listCliente)) {
            int id = 0;
            for (ClienteDto cliente : listCliente) {
                id++;
                addFileFolder(zos, cliente.getDocumentoIdentita(), cliente.getIdTipCliente().getDescrizione().concat("/").concat(String.valueOf(id)).concat("/").concat("DocumentoIdentita/"));
                addFileFolder(zos, cliente.getDocumentoIdentitaLegaleRappresentante(), cliente.getIdTipCliente().getDescrizione().concat("/").concat(String.valueOf(id)).concat("/").concat("DocumentoIdentitaLegaleRappresentante/"));
                addFileFolder(zos, cliente.getDichiarazioneRedditi(), cliente.getIdTipCliente().getDescrizione().concat("/").concat(String.valueOf(id)).concat("/").concat("DichiarazioneRedditi/"));
                addFileFolder(zos, cliente.getAltriDocumenti(), cliente.getIdTipCliente().getDescrizione().concat("/").concat(String.valueOf(id)).concat("/").concat("AltriDocumenti/"));
                addFileFolder(zos, cliente.getBilancioCompletoUltimoEsercizioChiuso(), cliente.getIdTipCliente().getDescrizione().concat("/").concat(String.valueOf(id)).concat("/").concat("BilancioCompletoUltimoEsercizioChiuso/"));
                addFileFolder(zos, cliente.getBilancioProvvisorioEsercizioCorso(), cliente.getIdTipCliente().getDescrizione().concat("/").concat(String.valueOf(id)).concat("/").concat("BilancioProvvisorioEsercizioCorso/"));
                addFileFolder(zos, cliente.getVisuraCamerale(), cliente.getIdTipCliente().getDescrizione().concat("/").concat(String.valueOf(id)).concat("/").concat("VisuraCamerale/"));
                addFileFolder(zos, cliente.getVisuraCatastale(), cliente.getIdTipCliente().getDescrizione().concat("/").concat(String.valueOf(id)).concat("/").concat("VisuraCatastale/"));
            }

        }
    }

    private void addFileFolder(ZipOutputStream zos, List<DocumentoDto> listDoc, String path) throws IOException {
        byte[] buffer = new byte[1024];
        if (!CollectionUtils.isEmpty(listDoc)) {
            for (DocumentoDto documentoDto : listDoc) {
                zos.putNextEntry(new ZipEntry(path.concat(documentoDto.getFilename())));
                URL u = new URL(documentoDto.getPathDoc());
                InputStream is = u.openStream();
                int length;
                while ((length = is.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }
                zos.closeEntry();
                is.close();
            }
        }
    }
}
