package it.gecotechsrl.bondbroker.app.utils;

import it.gecotechsrl.bondbroker.app.dao.*;
import it.gecotechsrl.bondbroker.app.model.*;
import it.gecotechsrl.bondbroker.dto.ClienteDto;
import it.gecotechsrl.bondbroker.dto.DocumentoDto;
import it.gecotechsrl.bondbroker.dto.PraticaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class ConverterUtils {
    Logger logger = LoggerFactory.getLogger(ConverterUtils.class);
    @Autowired
    private DocumentoClienteRepository documentoClienteRepository;

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ClientePraticaRepository clientePraticaRepository;

    @Autowired
    private PraticaRepository praticaRepository;
    @Autowired
    private DocumentoPraticaRepository documentoPraticaRepository;

    public PraticaDto getPraticaDto(Integer idPratica) {
        PraticaDto praticaDto = new PraticaDto();
        try {
            Optional<Pratica> pratica = praticaRepository.findById(idPratica);
            if (pratica.isPresent()) {
                praticaDto = convertPraticaTable(pratica.get());
                // RITROVA DOCUMENTO_PRATICA
                List<DocumentoPratica> documentoPraticaList = documentoPraticaRepository.findDocumentoPraticaByIdPratica(pratica.get().getId());
                if (!CollectionUtils.isEmpty(documentoPraticaList)) {
                    for (DocumentoPratica documentoPratica : documentoPraticaList) {
                        praticaDto.getDocumentazioneTecnica().add(convertDocumentoPratica(documentoPratica));
                    }
                }
                // CLIENTE PRATICA
                List<ClientePratica> clientePraticaList = clientePraticaRepository.findAllByIdPratica(pratica.get().getId());
                if (!CollectionUtils.isEmpty(clientePraticaList)) {
                    praticaDto = convertClientePratica(clientePraticaList, praticaDto);
                }
            }
            return praticaDto;
        } catch (Exception e) {
            logger.error("GET PRATICA BY ID : {}", e.getMessage(), e);
            return null;
        }

    }

    public DocumentoDto convertDocumentoPratica(DocumentoPratica documentoPratica) {
        DocumentoDto documentoDto = new DocumentoDto();
        documentoDto.setFilename(documentoPratica.getFilename());
        documentoDto.setId(documentoPratica.getId());
        documentoDto.setPathDoc(documentoPratica.getPathDoc());
        documentoDto.setTipoDoc(documentoPratica.getTipoDoc());
        return documentoDto;
    }

    public ClienteDto convertCliente(Cliente cliente, ClientePratica clientePratica) {
        ClienteDto clienteDto = new ClienteDto();
        clienteDto.setId(cliente.getId());
        clienteDto.setIdTipCliente(cliente.getIdTipCliente());
        if (Objects.nonNull(cliente.getNome())) clienteDto.setNome(cliente.getNome());
        if (Objects.nonNull(cliente.getCognome())) clienteDto.setCognome(cliente.getCognome());
        if (Objects.nonNull(cliente.getRagioneSociale())) clienteDto.setRagioneSociale(cliente.getRagioneSociale());
        if (Objects.nonNull(cliente.getPiva())) clienteDto.setPiva(cliente.getPiva());
        if (Objects.nonNull(cliente.getPec())) clienteDto.setPec(cliente.getPec());
        if (Objects.nonNull(cliente.getCf())) clienteDto.setCf(cliente.getCf());
        if (Objects.nonNull(cliente.getIndirizzo())) clienteDto.setIndirizzo(cliente.getIndirizzo());
        if (Objects.nonNull(cliente.getCivico())) clienteDto.setCivico(cliente.getCivico());
        if (Objects.nonNull(cliente.getCitta())) clienteDto.setCitta(cliente.getCitta());
        if (Objects.nonNull(cliente.getProvincia())) clienteDto.setProvincia(cliente.getProvincia());
        if (Objects.nonNull(cliente.getCap())) clienteDto.setCap(cliente.getCap());
        List<DocumentoCliente> documentoClienteList = documentoClienteRepository.findDocumentoClienteByIdCliente(cliente);
        if (!CollectionUtils.isEmpty(documentoClienteList))
            clienteDto = convertDocumentoCliente(documentoClienteList, clienteDto);
        if (Objects.nonNull(clientePratica)) clienteDto.setIdClientePratica(clientePratica.getId());
        if (Objects.nonNull(clientePratica)) clienteDto.setStato(clientePratica.getStato());
        return clienteDto;
    }

    private ClienteDto convertDocumentoCliente(List<DocumentoCliente> documentoClienteList, ClienteDto clienteDto) {
        for (DocumentoCliente documentoCliente : documentoClienteList) {
            switch (documentoCliente.getIdTipDocumento().getId()) {
                case 2:
                    clienteDto.getDocumentoIdentita().add(convertDocumentoCliente(documentoCliente));
                    break;
                case 3:
                    clienteDto.getDichiarazioneRedditi().add(convertDocumentoCliente(documentoCliente));
                    break;
                case 4:
                    clienteDto.getDocumentoIdentitaLegaleRappresentante().add(convertDocumentoCliente(documentoCliente));
                    break;
                case 5:
                    clienteDto.getBilancioCompletoUltimoEsercizioChiuso().add(convertDocumentoCliente(documentoCliente));
                    break;
                case 6:
                    clienteDto.getBilancioProvvisorioEsercizioCorso().add(convertDocumentoCliente(documentoCliente));
                    break;
                case 7:
                    clienteDto.getVisuraCatastale().add(convertDocumentoCliente(documentoCliente));
                    break;
                case 8:
                    clienteDto.getVisuraCamerale().add(convertDocumentoCliente(documentoCliente));
                    break;
                case 9:
                    clienteDto.getAltriDocumenti().add(convertDocumentoCliente(documentoCliente));
                    break;
            }
        }
        return clienteDto;
    }

    private DocumentoDto convertDocumentoCliente(DocumentoCliente documentoCliente) {
        DocumentoDto documentoDto = new DocumentoDto();
        documentoDto.setFilename(documentoCliente.getFilename());
        documentoDto.setId(documentoCliente.getId());
        documentoDto.setPathDoc(documentoCliente.getPathDoc());
        documentoDto.setTipoDoc(documentoCliente.getTipoDoc());
        return documentoDto;
    }

    public PraticaDto convertPraticaTable(Pratica pratica) {
        PraticaDto praticaDto = new PraticaDto();
        praticaDto.setId(pratica.getId());
        praticaDto.setIdTipGaranzia(pratica.getIdTipGaranzia());
        praticaDto.setOggetto(pratica.getOggetto());
        praticaDto.setNote(pratica.getNote());
        praticaDto.setValidoDa(pratica.getValidoDa());
        praticaDto.setValidoA(pratica.getValidoA());
        praticaDto.setDurataVal(pratica.getDurataVal());
        praticaDto.setExtGaranziaDa(pratica.getExtGaranziaDa());
        praticaDto.setExtGaranziaA(pratica.getExtGaranziaA());
        praticaDto.setDurataExt(pratica.getDurataExt());
        praticaDto.setImporto(pratica.getImporto());
        praticaDto.setTasso(pratica.getTasso());
        praticaDto.setPremio(pratica.getPremio());
        praticaDto.setValoreProgetto(pratica.getValoreProgetto());
        praticaDto.setIdTipFirma(pratica.getIdTipFirma());
        praticaDto.setIdTipStato(pratica.getIdTipStato());
        praticaDto.setOwner(pratica.getOwner());
        praticaDto.setStato(pratica.getStato());
        praticaDto.setDataModifica(pratica.getDataModifica());
        return praticaDto;
    }

    public PraticaDto convertClientePratica(List<ClientePratica> clientePraticaList, PraticaDto praticaDto) {
        for (ClientePratica clientePratica : clientePraticaList) {
            switch (clientePratica.getIdTipCliente()) {
                case 1:
                    praticaDto.getContraente().add(convertCliente(clienteRepository.findById(clientePratica.getIdCliente()).get(), clientePratica));
                    break;
                case 2:
                    praticaDto.getBeneficiario().add(convertCliente(clienteRepository.findById(clientePratica.getIdCliente()).get(), clientePratica));
                    break;
                case 3:
                    praticaDto.getCoobbligato().add(convertCliente(clienteRepository.findById(clientePratica.getIdCliente()).get(), clientePratica));
                    break;
            }
        }
        return praticaDto;
    }


}
