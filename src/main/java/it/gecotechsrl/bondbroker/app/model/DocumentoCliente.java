package it.gecotechsrl.bondbroker.app.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "documento_cliente")
@Data
public class DocumentoCliente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "tipo_doc")
    private String tipoDoc;
    @OneToOne
    @JoinColumn(name = "id_cliente")
    private Cliente idCliente;
    @OneToOne
    @JoinColumn(name = "id_tip_documento")
    private TipDocumento idTipDocumento;
    private String filename;
    @Column(name = "path_doc")
    private String pathDoc;
}
