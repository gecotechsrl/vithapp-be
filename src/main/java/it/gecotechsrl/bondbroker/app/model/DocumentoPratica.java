package it.gecotechsrl.bondbroker.app.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "documento_pratica")
@Data
public class DocumentoPratica implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "tipo_doc")
    private String tipoDoc;
    @Column(name = "id_pratica")
    private Integer idPratica;
    @OneToOne
    @JoinColumn(name = "id_tip_documento")
    private TipDocumento idTipDocumento;
    private String filename;
    @Column(name = "path_doc")
    private String pathDoc;
}
