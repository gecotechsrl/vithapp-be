package it.gecotechsrl.bondbroker.app.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "provincies")
@Data
public class Provincies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "sigla")
    private String sigla;
    private String provincia;
    @Column(name = "id_regione")
    private Integer idRegione;
}
