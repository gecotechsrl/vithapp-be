package it.gecotechsrl.bondbroker.app.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "cap")
@Data
public class Cap implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "istat")
    private Integer istat;
    private String cap;
}
