package it.gecotechsrl.bondbroker.app.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cliente_pratica")
@Data
public class ClientePratica implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_pratica")
    private Integer idPratica;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Column(name = "id_tip_cliente")
    private Integer idTipCliente;
    private int stato;
}
