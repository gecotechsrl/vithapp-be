package it.gecotechsrl.bondbroker.app.model;

public interface ComuniIntefarce {
    Integer getIstat();
    String getComune();
    String getProvincia();
    String getCap();

}
