package it.gecotechsrl.bondbroker.app.model;

import it.gecotechsrl.bondbroker.jwt.model.User;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "pratica")
@Data
public class Pratica implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @OneToOne
    @JoinColumn(name = "id_tip_garanzia")
    private TipGaranzia idTipGaranzia;
    private String oggetto;
    private String note;
    @Column(name = "valido_da")
    private LocalDate validoDa;
    @Column(name = "valido_a")
    private LocalDate validoA;
    @Column(name = "durata_val")
    private int durataVal;
    @Column(name = "ext_garanzia_da")
    private LocalDate extGaranziaDa;
    @Column(name = "ext_garanzia_a")
    private LocalDate extGaranziaA;
    @Column(name = "durata_ext")
    private int durataExt;
    private double importo;
    private double tasso;
    private double premio;
    @Column(name = "valore_progetto")
    private double valoreProgetto;
    @OneToOne
    @JoinColumn(name = "id_tip_firma")
    private TipFirma idTipFirma;
    @OneToOne
    @JoinColumn(name = "id_tip_stato")
    private TipStato idTipStato;
    @OneToOne
    @JoinColumn(name = "id_owner_user")
    private User owner;
    private int stato;
    @Column(name = "ultima_mod")
    private LocalDateTime dataModifica;
}
