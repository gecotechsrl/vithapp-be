package it.gecotechsrl.bondbroker.app.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cities")
@Data
public class Cities implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "istat")
    private Integer istat;
    private String comune;
    private String regione;
    private String provincia;
    private String prefisso;
    @Column(name = "cod_fisco")
    private String codFisco;
    @Column(name = "num_residenti")
    private Integer numResidenti;
}
