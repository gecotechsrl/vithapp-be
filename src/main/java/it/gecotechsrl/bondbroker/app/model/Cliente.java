package it.gecotechsrl.bondbroker.app.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cliente")
@Data
public class Cliente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id_tip_cliente")
    private TipCliente idTipCliente;

    private String nome;
    private String cognome;
    @Column(name = "ragione_sociale")
    private String ragioneSociale;
    private String cf;
    private String piva;
    private String pec;
    private String indirizzo;
    private String civico;
    private String cap;
    private String citta;
    private String provincia;
}
