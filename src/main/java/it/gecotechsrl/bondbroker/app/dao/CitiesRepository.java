package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.Cities;
import it.gecotechsrl.bondbroker.app.model.ComuniIntefarce;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface CitiesRepository extends JpaRepository<Cities, Integer> {
    @Query(value = "select c.istat, comune, provincia, cap from cities c, cap cap where c.istat = cap.istat and c.provincia = ?1 order by comune", nativeQuery = true)
    List<ComuniIntefarce> findAllComuniByCodProv(String siglaProv);

}
