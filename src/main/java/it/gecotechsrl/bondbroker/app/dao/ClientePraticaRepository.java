package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.Cliente;
import it.gecotechsrl.bondbroker.app.model.ClientePratica;
import it.gecotechsrl.bondbroker.app.model.Pratica;
import it.gecotechsrl.bondbroker.app.model.TipCliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface ClientePraticaRepository extends JpaRepository<ClientePratica, Integer> {
    List<ClientePratica> findAllByIdPratica (Integer idPratica);
    Optional<ClientePratica> findByIdClienteAndIdPraticaAndIdTipCliente(Integer idCliente, Integer idPratica, Integer idTipCliente);
}
