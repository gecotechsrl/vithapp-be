package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.DocumentoPratica;
import it.gecotechsrl.bondbroker.app.model.Pratica;
import it.gecotechsrl.bondbroker.app.model.TipDocumento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface DocumentoPraticaRepository extends JpaRepository<DocumentoPratica, Integer> {
    List<DocumentoPratica> findDocumentoPraticaByIdPratica(Integer idPratica);
    Optional<DocumentoPratica> findByIdPraticaAndFilename(Integer idPratica, String filename);
}
