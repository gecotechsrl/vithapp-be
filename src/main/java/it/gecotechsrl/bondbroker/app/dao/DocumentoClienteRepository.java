package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface DocumentoClienteRepository extends JpaRepository<DocumentoCliente, Integer> {
    List<DocumentoCliente> findDocumentoClienteByIdCliente(Cliente idCliente);
    List<DocumentoCliente> findDocumentoClienteByIdCliente_Id(Integer idCliente);
    Optional<DocumentoCliente> findByIdClienteAndIdTipDocumentoAndFilename(Cliente cliente, TipDocumento tipDocumento, String filename);
}
