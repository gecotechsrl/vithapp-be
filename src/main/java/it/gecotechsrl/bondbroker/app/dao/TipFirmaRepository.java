package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.TipFirma;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TipFirmaRepository extends JpaRepository<TipFirma, Integer> {
}
