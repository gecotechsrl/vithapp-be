package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.Pratica;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface PraticaRepository extends JpaRepository<Pratica, Integer> {
    Page<Pratica> findAllByOwner_IdIn(List<Integer> ownerId, Pageable pageable);
}
