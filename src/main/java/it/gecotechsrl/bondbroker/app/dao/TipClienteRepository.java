package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.TipCliente;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TipClienteRepository extends JpaRepository<TipCliente, Integer> {
}
