package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.Cap;
import it.gecotechsrl.bondbroker.app.model.Cities;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface CapRepository extends JpaRepository<Cap, Integer> {
    Optional<Cap> findByIstat(Integer codIstat);
}
