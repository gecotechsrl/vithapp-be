package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.TipStato;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TipStatoRepository extends JpaRepository<TipStato, Integer> {
}
