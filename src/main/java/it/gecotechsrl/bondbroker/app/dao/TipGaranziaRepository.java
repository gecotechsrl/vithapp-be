package it.gecotechsrl.bondbroker.app.dao;

import it.gecotechsrl.bondbroker.app.model.TipGaranzia;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TipGaranziaRepository extends JpaRepository<TipGaranzia, Integer> {
}
