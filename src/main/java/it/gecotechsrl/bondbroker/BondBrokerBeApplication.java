package it.gecotechsrl.bondbroker;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.IOException;

@EnableJpaRepositories
@SpringBootApplication
public class BondBrokerBeApplication extends SpringBootServletInitializer {
    private Logger applogger = LoggerFactory.getLogger(BondBrokerBeApplication.class);
    static final String FB_BASE_URL="https://bond-84a99.firebaseapp.com";
    static final String KEY_PATH="/bond-firebase-adminsdk.json";
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials
                            .fromStream(new ClassPathResource(KEY_PATH).getInputStream()))
                    .setDatabaseUrl(FB_BASE_URL).build();
            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
            }

        } catch (IOException e) {
            applogger.error("BondBrokerBeApplication Error : ", e);
        }
        return application.sources(BondBrokerBeApplication.class);
    }
    public static void main(String[] args) {
        SpringApplication.run(BondBrokerBeApplication.class, args);
    }
    }
