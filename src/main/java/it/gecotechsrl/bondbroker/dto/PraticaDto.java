package it.gecotechsrl.bondbroker.dto;

import it.gecotechsrl.bondbroker.app.model.TipFirma;
import it.gecotechsrl.bondbroker.app.model.TipGaranzia;
import it.gecotechsrl.bondbroker.app.model.TipStato;
import it.gecotechsrl.bondbroker.jwt.model.User;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class PraticaDto implements Serializable {
    private Integer id;
    private TipGaranzia idTipGaranzia;
    private String oggetto;
    private String note;
    private LocalDate validoDa;
    private LocalDate validoA;
    private int durataVal;
    private LocalDate extGaranziaDa;
    private LocalDate extGaranziaA;
    private int durataExt;
    private double importo;
    private double tasso;
    private double premio;
    private double valoreProgetto;
    private TipFirma idTipFirma;
    private TipStato idTipStato;
    private User owner;
    private String ownerName;
    private LocalDateTime dataModifica;
    private String contraenteTxt;
    private List<ClienteDto> contraente = new ArrayList();
    private List<ClienteDto> beneficiario = new ArrayList();
    private List<ClienteDto> coobbligato = new ArrayList();
    private List<DocumentoDto> documentazioneTecnica = new ArrayList();
    private Integer stato;
}
