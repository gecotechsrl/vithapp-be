package it.gecotechsrl.bondbroker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CompletableResp {
    private boolean esito;
    private String message;
    private Object object;
}
