package it.gecotechsrl.bondbroker.dto;

import lombok.Data;

@Data
public class ParamsTemplate {
    private String linkUrl;
    private String name;
    private String surname;
    private String email;
    private String date;

    public ParamsTemplate(String linkUrl, String name, String surname, String email, String date) {
        this.linkUrl = linkUrl;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.date = date;
    }

    public ParamsTemplate(String name, String surname, String email, String date) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.date = date;
    }
}
