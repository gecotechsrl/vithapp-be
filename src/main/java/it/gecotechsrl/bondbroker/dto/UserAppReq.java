package it.gecotechsrl.bondbroker.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UserAppReq {
    private int id;
	private String uid;
    private String name;
    private String surname;
    private String mail;
    private String password;
    private String cf;
    private LocalDate birthDate;
    private String phone;
    private String authorityName;
    private Integer idAdminOwner;
    private boolean enabled;
}
