package it.gecotechsrl.bondbroker.dto;

import lombok.Data;


@Data
public class FindPraticaDto {
    private Integer page;
    private Integer size;
    private Integer idUser;
    private String direction;
    private String column;
}
    

