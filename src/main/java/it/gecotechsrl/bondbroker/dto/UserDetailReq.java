package it.gecotechsrl.bondbroker.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UserDetailReq {
	private int id;
	private String name;
	private String surname;
    private String image;
	private String mail;
	private String cf;
	private LocalDate dataNascita;
	private String authorityName;
    private String tokenMobile;
	private String phone;
	private boolean primoAccesso;
	private Integer idControl;
	private Integer idGdpr;
	private Integer idTraguardo;
	private Integer idPresenter;
	private Integer idAgenzia;
	private boolean enabled;


}
