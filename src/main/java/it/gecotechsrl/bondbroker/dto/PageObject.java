package it.gecotechsrl.bondbroker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;


@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PageObject {
    private boolean hasMorePage;
    private int pageNumber;
    private int totalPage;
    private long totalCount;
    private List listObject;
}
    

