package it.gecotechsrl.bondbroker.dto;

import lombok.Data;


@Data
public class DocumentoDto {
    private Integer id;
    private String tipoDoc;
    private String filename;
    private String pathDoc;
}
