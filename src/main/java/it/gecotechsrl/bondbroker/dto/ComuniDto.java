package it.gecotechsrl.bondbroker.dto;

import lombok.Data;

import java.util.List;

@Data
public class ComuniDto {
    private Integer istat;
    private String comune;
    private String siglaProvincia;
    private List<String> cap;
}
