package it.gecotechsrl.bondbroker.dto;

import it.gecotechsrl.bondbroker.app.model.TipCliente;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class ClienteDto {
    private Integer id;
    private TipCliente idTipCliente;
    private String nome;
    private String cognome;
    private String ragioneSociale;
    private String cf;
    private String piva;
    private String pec;
    private String indirizzo;
    private String civico;
    private String cap;
    private String citta;
    private String provincia;
    private List<DocumentoDto> documentoIdentita= new ArrayList();
    private List<DocumentoDto> dichiarazioneRedditi= new ArrayList();
    private List<DocumentoDto> visuraCatastale= new ArrayList();
    private List<DocumentoDto> documentoIdentitaLegaleRappresentante= new ArrayList();
    private List<DocumentoDto> bilancioCompletoUltimoEsercizioChiuso= new ArrayList();
    private List<DocumentoDto> bilancioProvvisorioEsercizioCorso= new ArrayList();
    private List<DocumentoDto> visuraCamerale= new ArrayList();
    private List<DocumentoDto> altriDocumenti= new ArrayList();
    private Integer idClientePratica;
    private Integer stato;
    
}
