package it.gecotechsrl.bondbroker.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class NewUserDto {
    private Integer id;
    private String nome;
    private String cognome;
    private String link;
    
}
